#!/usr/bin/env python
"""
cmd_one_joint.py
James Watson, 2016 April 
Give one joint a position command
"""
import rospy # You need to import rospy if you are writing a ROS Node.
import math
from std_msgs.msg import String # The std_msgs.msg import is so that we can reuse the std_msgs/String message type 
#                                 (a simple string container) for publishing
from osrf_msgs.msg import JointCommands # See if this imports
from std_msgs.msg import Float64

#http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers
#pub = rospy.Publisher('topic_name', std_msgs.msg.String, queue_size=10)
#pub.publish(std_msgs.msg.String("foo"))

# URL, KUKA Joint Limits: http://www.dis.uniroma1.it/~labrob/pub/papers/ICRA14_LWR_RevMod.pdf
class LBR4info(object):
    jointLim = []
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 0
    jointLim.append( [ math.radians(-120) , math.radians(120) ] ) # Joint 1
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 2
    jointLim.append( [ math.radians(-120) , math.radians(120) ] ) # Joint 3
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 4
    jointLim.append( [ math.radians(-120) , math.radians(120) ] ) # Joint 5
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 6

def one_jnt_controller():
    # Create a new Publisher to the joint controller topic so that we can move the joint
    #pub = rospy.Publisher('/lbr_allegro/lbr4/joint3_position_controller/command', JointCommands, queue_size=10) # queue_size argument limits the amount of queued messages 
    #                                                                                                              if any subscriber is not receiving the them fast enough
    #pub = rospy.Publisher('allegro_hand_right/control/joint_cmd', JointCommands, queue_size=10)

    pub = []
    
    for joint_i in range(7): # Start a publisher node for each joint
        pub.append( rospy.Publisher('/lbr_allegro/lbr4/joint'+str(joint_i)+'_position_controller/command', Float64, queue_size=10) )
    
    rospy.init_node('lbr4_jnts', anonymous=True) # rospy.init_node(NAME), tells rospy the name of your node.
    #                                              Until rospy has this information, it cannot start communicating with the ROS Master
    refreshRate = 10
    pubClock = rospy.Rate(refreshRate) # 10hz, we should expect to go through the loop 10 times per second 
    #                                    (as long as our processing time does not exceed 1/10th of a second!)
    radsPerSec = [2 * math.pi / 12 for i in range(7)] #10 #2 * math.pi / 6 # sixth of a circle every second
    
    jntPos = [0 for i in range(7)]
    
    while not rospy.is_shutdown(): # check that no shutdown signal was recieved, and then do work
        hello_str = "hello world %s" % rospy.get_time() # create a String msg with a timestamp
        rospy.loginfo(hello_str) # the messages get printed to screen, it gets written to the Node's log file, and it gets written to rosout
        
        for joint_i in range(7):
            jntPos[joint_i] += radsPerSec[joint_i] / refreshRate
            
            if jntPos[joint_i] < LBR4info.jointLim[joint_i][0] or jntPos[joint_i] > LBR4info.jointLim[joint_i][1]: # if the joint is at a limit
                radsPerSec[joint_i] = -radsPerSec[joint_i] # reverse
                
            command = Float64(jntPos[joint_i])
        
            pub[joint_i].publish(command) # publishes to chatter topic using a newly created String message. 
        
        # initialize JointCommands message
        #command = JointCommands()
        #command.name = ["joint0"] #list(atlasJointNames)
        #n = len(command.name)
        #command.position     = jnt0pos #Float64(jnt0pos) # zeros(n)
        #command.velocity     = radsPerSec #Float64(radsPerSec) # zeros(n)
        #command.effort       = zeros(n) # already tuned
        #command.kp_position  = zeros(n) # already tuned
        #command.ki_position  = zeros(n) # already tuned
        #command.kd_position  = zeros(n) # already tuned
        #command.kp_velocity  = zeros(n) # already tuned
        #command.i_effort_min = zeros(n) # already tuned
        #command.i_effort_max = zeros(n) # already tuned        
        
        
        pubClock.sleep() # sleeps just long enough to maintain the desired rate through the loop
  
if __name__ == '__main__':
    try:
        one_jnt_controller()
    except rospy.ROSInterruptException:
        # catches a rospy.ROSInterruptException exception, which can be thrown by rospy.sleep() and rospy.Rate.sleep() 
        # methods when Ctrl-C is pressed or your Node is otherwise shutdown. The reason this exception is raised is 
        # so that you don't accidentally continue executing code after the sleep()
        pass
      
"""
# Joint Command Message
# This structure contains the gains to be applied to a joint.
# The controller is a PID with feedforward desired torque:
#
#   kp_position     * ( position - measured_position )       +
#   ki_position     * 1/s * ( position - measured_position ) +
#   kd_position     * s * ( position - measured_position ) +
#   kp_velocity    * ( velocity - measured_velocity )     +
#   effort
#
Header header

string[] name
float64[] position
float64[] velocity
float64[] effort

float64[] kp_position
float64[] ki_position
float64[] kd_position
float64[] kp_velocity

float64[] i_effort_min
float64[] i_effort_max
"""