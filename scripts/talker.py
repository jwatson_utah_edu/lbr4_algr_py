#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

## Simple talker demo that published std_msgs/Strings messages
## to the 'chatter' topic

import rospy # You need to import rospy if you are writing a ROS Node.
from std_msgs.msg import String # The std_msgs.msg import is so that we can reuse the std_msgs/String message type 
#                                 (a simple string container) for publishing

def talker():
	# declares that node is publishing to the chatter topic using the message type std_msgs.msg.String
    pub = rospy.Publisher('chatter', String, queue_size=10) # queue_size argument limits the amount of queued messages 
#                                                           if any subscriber is not receiving the them fast enough
    rospy.init_node('talker', anonymous=True) # rospy.init_node(NAME), tells rospy the name of your node.
#                                               Until rospy has this information, it cannot start communicating with the ROS Master
    rate = rospy.Rate(10) # 10hz, we should expect to go through the loop 10 times per second 
#                                 (as long as our processing time does not exceed 1/10th of a second!)
    while not rospy.is_shutdown(): # check that no shutdown signal was recieved, and then do work
        hello_str = "hello world %s" % rospy.get_time() # create a String msg with a timestamp
        rospy.loginfo(hello_str) # the messages get printed to screen, it gets written to the Node's log file, and it gets written to rosout
        pub.publish(hello_str) # publishes to chatter topic using a newly created String message. 
        rate.sleep() # sleeps just long enough to maintain the desired rate through the loop

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
		# catches a rospy.ROSInterruptException exception, which can be thrown by rospy.sleep() and rospy.Rate.sleep() 
#         methods when Ctrl-C is pressed or your Node is otherwise shutdown. The reason this exception is raised is 
#         so that you don't accidentally continue executing code after the sleep()
        pass

