#!/usr/bin/env python
"""
joint_angle_query.py
James Watson, 2016 April 
Get a set of joint angles for a specified position
"""
import rospy # You need to import rospy if you are writing a ROS Node.
import math
from std_msgs.msg import String # The std_msgs.msg import is so that we can reuse the std_msgs/String message type 
#                                 (a simple string container) for publishing
from osrf_msgs.msg import JointCommands # See if this imports
from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray, MultiArrayDimension # For constructing a Float64MultiArray
import Tkinter

# URL, KUKA Joint Limits: http://www.dis.uniroma1.it/~labrob/pub/papers/ICRA14_LWR_RevMod.pdf
class LBR4info(object):
    jointLim = []
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 0
    jointLim.append( [ math.radians(-120) , math.radians(120) ] ) # Joint 1
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 2
    jointLim.append( [ math.radians(-120) , math.radians(120) ] ) # Joint 3
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 4
    jointLim.append( [ math.radians(-120) , math.radians(120) ] ) # Joint 5
    jointLim.append( [ math.radians(-170) , math.radians(170) ] ) # Joint 6



def init_and_run_node():
    newData = False
    print newData
    
    # Init graphics
    topWin = Tkinter.Tk() # Main tkinter window
    # Init entry labels
    Tkinter.Label(topWin, text="Desired X").grid(row=0, column=0)
    Tkinter.Label(topWin, text="Desired Y").grid(row=0, column=1)
    Tkinter.Label(topWin, text="Desired Z").grid(row=0, column=2)
    # Init entry boxes
    xEntry = Tkinter.Entry(topWin)
    yEntry = Tkinter.Entry(topWin)
    zEntry = Tkinter.Entry(topWin)
    # Grid the entry boxes
    xEntry.grid(row=1, column=0) # NEVER MIX 'grid' AND 'pack'!
    yEntry.grid(row=1, column=1)
    zEntry.grid(row=1, column=2)
    
    # Setup angle request button
    def bttn_press():
        """ Button press response, fetches a set of angles based on user data """
        global newData
        print 'pressed!' # works        
        newData = True 
        print "New data!"
        #posArr = [ Float64( xEntry.get() ) , Float64( yEntry.get() ) , Float64( zEntry.get() ) ]
        posArr = [ float( xEntry.get() ) , float( yEntry.get() ) , float( zEntry.get() ) ] # Fetch the user-requested position
        #create the MultiArray to be published
        dim = MultiArrayDimension()
        dim.size = len(posArr)
        dim.label = "posMsg"
        dim.stride = len(posArr)
         
        msgArr = Float64MultiArray()
        msgArr.data = posArr
        msgArr.layout.dim.append(dim)
        msgArr.layout.data_offset = 0
        print msgArr.data
        
        pub.publish(msgArr) # Publish the array or desired coordinates
        sendLabel.config(text='Msg Sent: '+str(sent))
        
        newData = False
        
    reqBttn = Tkinter.Button(topWin, text="Request Angles", command=bttn_press)
    reqBttn.grid(row=1, column=3)
    
    # Setup the output labels and boxes
    jointDisplay = []
    jointHandles = [] # StringVar()
    for jointDex in range(6):
        Tkinter.Label(topWin, text = "Joint " + str(jointDex)).grid(row=2, column=jointDex)
        #jointDisplay.append( Tkinter.Entry(topWin) )
        jointHandles.append( Tkinter.StringVar() )
        jointDisplay.append( Tkinter.Entry(topWin, textvariable = jointHandles[jointDex]) )
        jointDisplay[jointDex].grid(row=3, column=jointDex)   

    # == Diagnostic Labels ==        
    
    # Test label
    timeSteps = 0
    countLabel = Tkinter.Label(topWin, text=str(timeSteps)) # URL, Reset the text of a label: http://www.python-course.eu/tkinter_labels.php
    countLabel.grid(row=0, column=3)
    
    sent = False
    sendLabel = Tkinter.Label(topWin, text='Msg Sent: '+str(sent))
    sendLabel.grid(row=0, column=4)
    # == End Diagnostic ==

    def fetch_joint_angles(data):
        for jointDex, angle in enumerate(data.data):
            print angle
            # Trying to change these input box contents causes Tkinter to puke: "RuntimeError: main thread is not in main loop"
            #jointHandles[jointDex].set( str(angle) ) # URL, Change text of an entry: http://effbot.org/tkinterbook/entry.htm
            #jointDisplay[jointDex].set_txt(  )

    # This node has a publisher and a subscriber
    pub = rospy.Publisher('lbr_allegro/lbr4/control/pos_request', Float64MultiArray, queue_size=10) # Publisher sends a position
    sub = rospy.Subscriber('lbr_allegro/lbr4/control/angles_only', Float64MultiArray, fetch_joint_angles) # Subscriber receives an array of joint angles
    
    rospy.init_node('lbr4_jnt_angle_calc', anonymous=True) # rospy.init_node(NAME), tells rospy the name of your node.
    #                                              Until rospy has this information, it cannot start communicating with the ROS Master
    refreshRate = 10
    pubClock = rospy.Rate(refreshRate) # 10hz, we should expect to go through the loop 10 times per second 
    #                                    (as long as our processing time does not exceed 1/10th of a second!)
    
       
    
    while not rospy.is_shutdown(): # check that no shutdown signal was recieved, and then do work
        #hello_str = "hello world %s" % rospy.get_time() # create a String msg with a timestamp
        #rospy.loginfo(hello_str) # the messages get printed to screen, it gets written to the Node's log file, and it gets written to rosout
    
        timeSteps += 1
        #if timeSteps % 10 == 0:
        countLabel.config(text='Time:' + str(timeSteps/10.0))
        topWin.update_idletasks() # Make the graphics window heartbeat the same as the node heartbeat
        topWin.update() #           URL: http://stackoverflow.com/a/29158947/893511
        pubClock.sleep() # sleeps just long enough to maintain the desired rate through the loop

if __name__ == '__main__':
    try:
        init_and_run_node()
    except rospy.ROSInterruptException:
        # catches a rospy.ROSInterruptException exception, which can be thrown by rospy.sleep() and rospy.Rate.sleep() 
        # methods when Ctrl-C is pressed or your Node is otherwise shutdown. The reason this exception is raised is 
        # so that you don't accidentally continue executing code after the sleep()
        pass